//
//  DetailViewController.m
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/19/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        NSDictionary *jobListing = self.detailItem;
        NSString *jobTitle = jobListing[@"title"];
        NSString *jobDescription = jobListing[@"description"];
        NSString *companyName = jobListing[@"company"];
        id companyUrlEntry = jobListing[@"company_url"];
        NSString *companyUrl = (companyUrlEntry == [NSNull null] ? nil : companyUrlEntry);
        NSString *jobDetailUrl = jobListing[@"url"];
        NSMutableString *html = [[NSMutableString alloc] initWithString:@"<html><head>"];
        [html appendString:@"<style>"];
        [html appendString:@"body { font-family: \"Helvetica Neue\", Helvetica, sans-serif; }"];        
        [html appendString:@"</style></head><body>"];
        [html appendFormat:@"<h1>%@</h1>", jobTitle];
        [html appendFormat:@"<h2>%@</h2>", companyName];
        [html appendFormat:@"<div class=\"description\">%@</div>", jobDescription];
        if (companyUrl != nil) {
            [html appendFormat:@"<p><a href=\"%@\">Visit the %@ website</a></p>", companyUrl, companyName];
        }
        [html appendFormat:@"<p><a href=\"%@\">View this job at jobs.github.com</a></p>", jobDetailUrl];
        [html appendString:@"</body></html>"];
        [self.detailDescriptionWebView loadHTMLString:html baseURL:nil];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.detailDescriptionWebView.delegate = self;
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.detailDescriptionWebView.delegate = nil;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    BOOL result;
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        // open the link in Safari instead
        [[UIApplication sharedApplication] openURL:request.URL];
        result = NO;
    }
    else {
        result = YES;
    }
    
    return result;
}

@end
