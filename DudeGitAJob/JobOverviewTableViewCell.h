//
//  JobOverviewTableViewCell.h
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/21/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobOverviewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *jobTitle;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UIImageView *companyLogo;
//@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *companyLogoLoadingActivityIndicator;

@end
