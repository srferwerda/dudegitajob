//
//  MasterViewController.h
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/19/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class DetailViewController;

@interface MasterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (weak, nonatomic) IBOutlet UITextField *searchDescriptionField;
@property (weak, nonatomic) IBOutlet UITextField *searchLocationField;
@property (weak, nonatomic) IBOutlet UISwitch *fulltimeOnlySwitch;
@property (weak, nonatomic) IBOutlet UISwitch *useLocationServicesSwitch;
@property (weak, nonatomic) IBOutlet UILabel *refreshingLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *refreshingActivityIndicator;

- (IBAction)searchButtonTouched:(id)sender;
- (IBAction)locationServicesSwitchChanged:(id)sender;

@end

