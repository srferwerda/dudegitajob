//
//  GithubJobsAPIClient.m
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/21/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import "GithubJobsAPIClient.h"
#import <CoreLocation/CoreLocation.h>

@implementation GithubJobsAPIClient

static NSString * const GithubJobsAPIBaseURLString = @"https://jobs.github.com/";


+ (instancetype)sharedClient {
    static GithubJobsAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:GithubJobsAPIBaseURLString]];
//        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    return _sharedClient;
}

//- (NSURLSessionDataTask *)GET:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure
- (NSURLSessionDataTask *)fetchJobsMatchingKeywords:(NSString *)keywords withOnlyFulltimeStatus:(BOOL)onlyFulltime nearLocation:(id)locationTerm success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    /*
     GET /positions.json
     
     Search for jobs by term, location, full time vs part time, or any combination of the three. All parameters are optional.
     
     Parameters
     
     description — A search term, such as "ruby" or "java". This parameter is aliased to search.
     location — A city name, zip code, or other location search term.
     lat — A specific latitude. If used, you must also send long and must not send location.
     long — A specific longitude. If used, you must also send lat and must not send location.
     full_time — If you want to limit results to full time positions set this parameter to 'true'.

     */
    NSString *positionsUrl = [[NSURL URLWithString:@"positions.json" relativeToURL:self.baseURL] absoluteString];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (keywords.length > 0) {
        params[@"description"] = keywords;
    }
    if (onlyFulltime) {
        params[@"full_time"] = @"true";
    }
    if (locationTerm != nil) {
        if ([locationTerm isKindOfClass:[CLLocation class]]) {
            NSNumber *latNbr = [NSNumber numberWithDouble:((CLLocation *)locationTerm).coordinate.latitude];
            NSNumber *longNbr = [NSNumber numberWithDouble:((CLLocation *)locationTerm).coordinate.longitude];
            params[@"lat"] = latNbr.stringValue;
            params[@"long"] = longNbr.stringValue;
        }
        else if ([locationTerm isKindOfClass:[NSString class]]) {
            params[@"location"] = locationTerm;
        }
        else {
            NSAssert(NO, @"unknown data type passed for location search parameter");
        }
    }
    return [self GET:positionsUrl parameters:params success:success failure:failure];
}

@end
