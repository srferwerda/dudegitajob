//
//  JobOverviewTableViewCell.m
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/21/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import "JobOverviewTableViewCell.h"

@implementation JobOverviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
