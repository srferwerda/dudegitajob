//
//  DetailViewController.h
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/19/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) id detailItem;
//@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIWebView *detailDescriptionWebView;

@end

