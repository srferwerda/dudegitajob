//
//  MasterViewController.m
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/19/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "JobOverviewTableViewCell.h"
#import "GithubJobsAPIClient.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <TMCache/TMCache.h>

@interface MasterViewController ()

@property NSMutableArray *viewModel;
@property CLLocationManager *locationManager;
//@property CLLocation *currentLocation;

@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        //self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    // force location services off until we have a location
//    self.useLocationServicesSwitch.hidden = (self.currentLocation == nil);
//    
    // setup for detail view controller
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    // do location services check and init
    if ([CLLocationManager locationServicesEnabled]) {
        if (self.locationManager == nil) {
            self.locationManager = [[CLLocationManager alloc] init];
        }
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        self.locationManager.distanceFilter = 500; // half-kilometer
        [self.locationManager startUpdatingLocation];
    }
    
    // check cache for previous search
    [[TMDiskCache sharedCache] objectForKey:@"viewModel" block:^(TMDiskCache *cache, NSString *key, id<NSCoding> object, NSURL *fileURL) {
        self.viewModel = (NSMutableArray *)object;
        [self.tableView reloadData];
    }];
    self.searchDescriptionField.text = (NSString *)[[TMDiskCache sharedCache] objectForKey:@"keywords"];
    self.searchLocationField.text = (NSString *)[[TMDiskCache sharedCache] objectForKey:@"location"];
    self.fulltimeOnlySwitch.on = ((NSNumber *)[[TMDiskCache sharedCache] objectForKey:@"fullTimeOnly"]).boolValue;
    self.useLocationServicesSwitch.on = ((NSNumber *)[[TMDiskCache sharedCache] objectForKey:@"useLocationServices"]).boolValue;
    
    // check for location services allowed
    if ([CLLocationManager locationServicesEnabled] &&([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)) {
        self.useLocationServicesSwitch.enabled = YES;
    }
    else {
        self.useLocationServicesSwitch.on = NO;
        self.useLocationServicesSwitch.enabled = NO;
    }
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *object = self.viewModel[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JobOverviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OverviewCell" forIndexPath:indexPath];

    NSDictionary *jobListing = self.viewModel[indexPath.row];
    cell.jobTitle.text = jobListing[@"title"];
    cell.companyName.text = jobListing[@"company"];
    cell.companyLogo.image = nil;
    id logoUrlEntry = jobListing[@"company_logo"];
    if ((logoUrlEntry != nil) && ([logoUrlEntry isKindOfClass:[NSNull class]] == NO)) {
        NSString *logoUrlString = logoUrlEntry;
        if (logoUrlString.length > 0) {
            DDLogDebug(@"START logo load for row %ld, company %@", (long)indexPath.row, (NSString *)(jobListing[@"company"]));
            UIImage *placeholderImage = [UIImage imageNamed:@"logo-placeholder"];
            [cell.companyLogo sd_setImageWithURL:[NSURL URLWithString:logoUrlString] placeholderImage:placeholderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                DDLogDebug(@"FINISH logo load for row %ld, company %@", (long)indexPath.row, (NSString *)(jobListing[@"company"]));
            }];
        }
        else {
            DDLogDebug(@"Logo URL too short for row %ld, company %@", (long)indexPath.row, (NSString *)(jobListing[@"company"]));
        }
    }
    else {
        DDLogDebug(@"No logo URL found for row %ld, company %@", (long)indexPath.row, (NSString *)(jobListing[@"company"]));
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.searchDescriptionField resignFirstResponder];
    [self.searchLocationField resignFirstResponder];
    // push to detail
    [self performSegueWithIdentifier:@"showDetail" sender:tableView];
}

#pragma mark - target/action

- (IBAction)searchButtonTouched:(id)sender {
    [self.searchDescriptionField resignFirstResponder];
    [self.searchLocationField resignFirstResponder];
    self.refreshingLabel.hidden = NO;
    self.refreshingActivityIndicator.hidden = NO;
    [self.refreshingActivityIndicator startAnimating];
    
    NSString *searchKeywords = self.searchDescriptionField.text;
    NSString *searchLocation = self.searchLocationField.text;
    BOOL searchFullTimeJobsOnly = self.fulltimeOnlySwitch.on;
    BOOL searchUsingLocationServices = self.useLocationServicesSwitch.on;
    id locationTerm;
    if (searchUsingLocationServices) {
        locationTerm = self.locationManager.location;
    }
    else {
        locationTerm = searchLocation;
    }
    
    DDLogDebug(@"BEGIN fetch of Github jobs data");
    [[GithubJobsAPIClient sharedClient] fetchJobsMatchingKeywords:searchKeywords withOnlyFulltimeStatus:searchFullTimeJobsOnly nearLocation:locationTerm success:^(NSURLSessionDataTask *task, id response) {
        DDLogDebug(@"END fetch of Github jobs data - success");
        if ([self.tableView numberOfSections] > 0 && [self.tableView numberOfRowsInSection:0] > 0) {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
        self.viewModel = [[NSMutableArray alloc] initWithArray:(NSArray *)response];
        [[TMDiskCache sharedCache] setObject:self.viewModel forKey:@"viewModel"];
        NSString *cachedKeywords = (searchKeywords.length > 0 ? searchKeywords : @"");
        [[TMDiskCache sharedCache] setObject:cachedKeywords forKey:@"keywords"];
        NSString *cachedLocation = (searchLocation.length > 0 ? searchLocation : @"");
        [[TMDiskCache sharedCache] setObject:cachedLocation forKey:@"location"];
        [[TMDiskCache sharedCache] setObject:[NSNumber numberWithBool:searchFullTimeJobsOnly] forKey:@"fullTimeOnly"];
        [[TMDiskCache sharedCache] setObject:[NSNumber numberWithBool:searchUsingLocationServices] forKey:@"useLocationServices"];
        self.refreshingLabel.hidden = YES;
        [self.refreshingActivityIndicator stopAnimating];
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        //
        DDLogDebug(@"END fetch of Github jobs data - failure");
        self.refreshingLabel.hidden = YES;
        [self.refreshingActivityIndicator stopAnimating];
        NSString *errorMessage = [[NSString alloc] initWithFormat:@"There was an error retrieving job data from Github Jobs.\n\n%@", error.localizedDescription];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Egad!" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }];
}

- (IBAction)locationServicesSwitchChanged:(id)sender {
    UISwitch *sw = (UISwitch *)sender;
    if (sw.on) {
        [self.locationManager startUpdatingLocation];
    }
    else {
        [self.locationManager stopUpdatingLocation];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
//    self.currentLocation = [locations lastObject];
}

@end
