//
//  GithubJobsAPIClient.h
//  DudeGitAJob
//
//  Created by Scott Ferwerda on 5/21/15.
//  Copyright (c) 2015 Scott Ferwerda. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface GithubJobsAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

- (NSURLSessionDataTask *)fetchJobsMatchingKeywords:(NSString *)keywords withOnlyFulltimeStatus:(BOOL)onlyFulltime nearLocation:(id)locationTerm success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure;

@end
