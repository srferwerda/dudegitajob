# README #

### What is this repository for? ###

* A simple demonstration app for retrieving and displaying job information from Github's jobs API.
* Version 0.1

### How do I get set up? ###

* This is a standard Xcode project, created in Xcode 6.3.1. It also uses several [Cocoapods](http://cocoapods.org)

